import React, { useState, useEffect } from 'react'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader, OrganisationUnitTree } from '@dhis2/ui'
import { NavLink, withRouter, useHistory, useParams } from "react-router-dom";
import {
    Box, Paper, Container, TextField, InputLabel, Snackbar,
    Grid, List, ListItem, Divider, CircularProgress,
    Dialog, DialogContent, DialogTitle, DialogActions,
    ListItemSecondaryAction, IconButton, InputAdornment,
    ListItemText, Typography, Button, MenuItem, Card, Select,
    FormHelperText
} from "@material-ui/core";
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ClearIcon from '@material-ui/icons/Clear';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { useDataMutation } from '@dhis2/app-runtime'
import DateFnsUtils from '@date-io/date-fns';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider
} from '@material-ui/pickers';
import { FormatListBulletedSharp } from '@material-ui/icons';

const ListaPrograms = {
    results: {
        resource: 'programs',
        id: 'xW2D1dak6g6',
        params: {
            fields: [['id,name,programStages[programStageDataElements[compulsory,dataElement[displayName,id]],programStageSections[id,name,dataElements[id,name,valueType,optionSet[id,name,options[id,name,code]]]]']],
            paging: false,
        },
    },
}

const SaveEvent = {
    resource: 'events',
    type: "create",
    data: ({ formEvent }) => formEvent,
}

const orgUnitQuery = {
    results: {
        resource: 'me.json',
        params: {
            fields: ['organisationUnits[id,name,code,level,created,path]'],
        }
    }
}

const fieldsMapping = [
    {
        mainDateElement: "KLWeSxlzFfk", masterDataElement: "pqkZ5eRXQeU", value: "1"
    },
    {
        mainDateElement: "Ys5yIsJUJD4", masterDataElement: "pqkZ5eRXQeU", value: "0"
    },
    {
        mainDateElement: "vDkQtqhzYbT", masterDataElement: "uM6X63gpZf9", value: "0"
    },
    {
        mainDateElement: "AbHxI6ztCbw", masterDataElement: "QBCGkrVdrCN", value: "0"
    },
    {
        mainDateElement: "HU05vCWp17q", masterDataElement: "EbpxmdSa192", value: "0"
    },
    {
        mainDateElement: "eOm4bNyTAm3", masterDataElement: "HcsdMnMtOur", value: "0"
    },
]
let formData = []
let masterMappings = []
let a = 0
function NewSite(props) {
    const engine = useDataEngine()
    const history = useHistory();
    const { loading, data } = useDataQuery(ListaPrograms)
    const [TodosDados, setTodosDados] = useState([])
    const [startDate, setstartDate] = useState(format(new Date(), 'yyyy-MM-dd'))
    const [state, setstate] = useState(true)
    const [state1, setstate1] = useState(false)
    const [program, setprogram] = useState("")
    const [programId, setprogramId] = useState("")
    const [orgUnits, setorgUnits] = useState("")
    const [orgUnitsId, setorgUnitsId] = useState("")
    const [orgUnitSelected, setorgUnitSelected] = useState([])
    const [initialPath, setinitialPath] = useState([])
    const [masterMapping, setmasterMapping] = useState([])

    const [mutate, response] = useDataMutation(SaveEvent, {
        onError: function (params) {
            setstate1(true)
        },
        onComplete: function (params) {
            props.messageBox('Salvo com sucesso!');
            props.setTabOrder(1)
            history.replace("/site");
        }
    })

    if (response.error?.details && state1) {
        props.messageBox(response.error?.details?.response.importSummaries[0].description)
        setstate1(false)
    }

    useEffect(() => {
        const programName = engine.query(ListaPrograms)
        const orgUnitTree = engine.query(orgUnitQuery)

        Promise.all([programName, orgUnitTree]).then((resp) => {
            setprogram(resp[0].results.name)
            setprogramId(resp[0].results.id)
            setorgUnits(resp[1]?.results.organisationUnits[0].name)
            setorgUnitsId(resp[1]?.results.organisationUnits[0].id)
            setinitialPath(resp[1]?.results.organisationUnits[0].path)
            resp[0].results.programStages[0].programStageDataElements.forEach(element => {
                element = ({ "id": element.dataElement.id, "compulsory": element.compulsory })

                masterMappings.push(element)
            });
            setmasterMapping(masterMappings)
            setstate(false)
        })
    }, [masterMapping])

    useEffect(() => {
        props.setdisabled(true)
        if (!loading) {
            data.results.programStages[0].programStageSections.forEach((element, i) => {
                element.dataElements.forEach((elem, j) => {
                    element.dataElements[j] = ({ ...elem, value: "" })
                });
            });
            setTodosDados(data.results.programStages[0].programStageSections)

        }
    }, [data])

    const organizar = (id, value, i, j) => {
        let newTodosDados = [...TodosDados]
        newTodosDados[i].dataElements[j].value = value
        setTodosDados(newTodosDados)
    }

    const SaveForm = async () => {
        let arrayForm = []
        let arraySave = []
        a = 0

        TodosDados.forEach((elemen, i) => {
            TodosDados[i].dataElements.forEach(element => {
                masterMapping.forEach(elem => {
                    if (element.id === elem.id) {
                        if (elem.compulsory === true) {
                            if (element.value == "") {
                                a = a + 1
                            }
                        }
                    }
                });

                arrayForm = ({
                    ...arrayForm, "dataElement": element.id,
                    "value": element.value
                })
                arraySave.push(arrayForm)

            });
        });

        if (a > 0) {
            return props.messageBox("Porfavor Preencha os Campos Obrigatorios")
        }

        let form = ({
            "program": programId,
            "orgUnit": orgUnitSelected.id,
            "eventDate": startDate,
            "dataValues": arraySave
        })
        if (a > 0) {

        } else if (orgUnitSelected.id) {
            await mutate({
                formEvent: form
            })

        } else {
            props.messageBox("Selecione uma Unidade Organizacional")
        }
        if (response.error) {
            props.messageBox(response.error?.details?.response.importSummaries[0].description)
        }

    }


    const checkId = (elem, section) => {
        for (let fieldMapping of fieldsMapping) {
            if (elem.id == fieldMapping.mainDateElement) {
                for (let sectionDataElement of section.dataElements) {
                    if (fieldMapping.masterDataElement == sectionDataElement.id) {
                        if (fieldMapping.value != sectionDataElement.value) {
                            return true
                        }
                    }
                }
            }
        }

        return false
    }
    const checkObrigatory = (elem, section) => {
        for (let fieldMapping of masterMapping) {
            if (elem.id === fieldMapping.id) {
                if (elem.value == "") {
                    if (fieldMapping.compulsory) {
                        return fieldMapping.compulsory
                    }
                }
            }
        }
        return false
    }

    if (state) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }

    return (
        <Container maxWidth="xl" style={{ margin: 10 }}>
            <br />
            <Box display="flex">
                <Box>
                    <IconButton component={NavLink} to="/site">
                        <ArrowLeftIcon />
                    </IconButton>
                </Box>
                <Box style={{ paddingTop: '7px', marginRight: 50 }} flexGrow={1} component={Typography} variant="h5">Criar Evento</Box>
            </Box>
            <br />

            {/* <Grid item xs style={{ width: 330, borderRadius: 0, marginRight: 70 }}>
                <Typography variant="button" display="block" style={{ paddingTop: '8px' }} variant="h6">
                    Unidade Organizacional
                                </Typography>
                <div style={{ overflow: "scroll", height: "340px", minWidth: "400px", marginTop: "15px" }}>
                </div>
            </Grid> */}
            {/* { console.log(orgUnitSelected)} */}

            {!(orgUnitSelected.length === 0) ?
                <Card elevation={0} >
                    <Grid item xs={12} sm container style={{ position: 'relative', margin: 40 }}>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs style={{ width: 330, borderRadius: 0, marginLeft: 70 }}>
                                <Typography variant="button" display="block" style={{ paddingTop: '8px' }} variant="h6" style={{ fontSize: 14 }}>
                                    Unidade Organizacional
                                </Typography>

                                <Grid container spacing={1} alignItems="flex-end">
                                    <Grid item>
                                        <TextField
                                            value={orgUnitSelected ? orgUnitSelected.displayName : ""} disabled/>
                                    </Grid>
                                    <Grid item>
                                        <IconButton size={"small"} onClick={() => setorgUnitSelected([])}>
                                            <ClearIcon />
                                        </IconButton>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Grid>
                        <Grid item style={{ width: 230, height: 80, borderRadius: 0, marginRight: 70 }}>
                            {/* <Card elevation={2} style={{ width: 230, height: 80, borderRadius: 0, textAlign: 'center', marginRight: 70 }}> */}
                            <Typography variant="button" display="block" variant="h6" style={{ fontSize: 14 }} >
                                Programa
                                </Typography>
                            <Typography style={{ minHeight: 120 }} variant="body2"
                                color="textSecondary" component="p">
                                <b>{program}</b>
                            </Typography>
                            {/* </Card> */}
                        </Grid>
                    </Grid>
                </Card>
                :
                <Card elevation={0} >
                    <Grid item xs={12} sm container style={{ position: 'relative', margin: 40 }}>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs style={{ width: 330, borderRadius: 0, marginRight: 70 }}>
                                <Typography variant="button" display="block" style={{ paddingTop: '8px' }} variant="h6">
                                    Unidade Organizacional
                                </Typography>
                                <div style={{ overflow: "scroll", height: "340px", minWidth: "400px", marginTop: "15px" }}>
                                    {orgUnitsId && <OrganisationUnitTree
                                        style={{ margin: 200 }}
                                        name={orgUnits}
                                        roots={orgUnitsId}
                                        initiallyExpanded={[initialPath]}
                                        onChange={(e) => setorgUnitSelected(e)}
                                        selected={orgUnitSelected.selected}
                                        singleSelection
                                    />}
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item style={{ width: 230, height: 80, borderRadius: 0, marginRight: 70, marginTop: 170 }}>
                            {/* <Card elevation={2} style={{ width: 230, height: 80, borderRadius: 0, textAlign: 'center', marginRight: 70 }}> */}
                            <Typography variant="button" display="block" variant="h6" style={{ fontSize: 12 }}>
                                Programa
                                </Typography>
                            <Typography style={{ minHeight: 120 }} variant="body2"
                                color="textSecondary" component="p">
                                <b>{program}</b>
                            </Typography>
                            {/* </Card> */}
                        </Grid>
                    </Grid>
                </Card>
            }
            <br />
            <Paper elevation={3} style={{ padding: '28px 70px 56px' }}>
                <Grid container spacing={2}>
                    <Grid item sm={12} style={{ margin: 10, marginTop: 20 }}>
                        <InputLabel style={{ marginTop: 20, marginBottom: 20 }}><b>{"INFORMAÇÁO BÁSICA"}</b></InputLabel>
                        <Card elevation={2} style={{ borderRadius: 0, marginTop: 20 }}>
                            <InputLabel style={{ marginTop: 10, marginLeft: 30 }}>{"Event Date"}</InputLabel>

                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    disableToolbar
                                    style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                    // // variant="inline"
                                    format="yyyy-MM-dd"
                                    margin="normal"
                                    id="date-picker-inline"
                                    // label={""}
                                    value={startDate}
                                    onChange={(date) => setstartDate(date)}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Card>
                    </Grid>

                    {TodosDados?.map((root, i) => (
                        <Grid item sm={12} style={{ margin: 10 }}>
                            <InputLabel style={{ marginTop: 20, marginBottom: 20 }}><b>{root?.name}</b></InputLabel>
                            <Card elevation={2} style={{ borderRadius: 0 }}>
                                {TodosDados[i].dataElements.map((row, j) => {

                                    let status = checkId(row, TodosDados[i])
                                    let obrigatory = checkObrigatory(row, TodosDados[i])
                                    return (
                                        <>
                                            {(row?.optionSet) ?
                                                <>
                                                    <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                    <Select
                                                        // select
                                                        defaultValue={row?.value}
                                                        id={row?.id}
                                                        error={obrigatory}
                                                        // disabled={view ? view : status}
                                                        value={row?.value}
                                                        // style={{ width: "96%" }}
                                                        style={{ width: "96%", marginLeft: 32 }}
                                                        onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                    >
                                                        {row?.optionSet.options.map((option) => (
                                                            <MenuItem key={option.code} value={option.code}>
                                                                {option.name}
                                                            </MenuItem>
                                                        ))}
                                                    </Select>
                                                    <FormHelperText style={{ width: "96%", marginLeft: 32, marginBottom: 10, color: "black" }}>{obrigatory && "Campo Obrigatorio"}</FormHelperText>
                                                </>
                                                :
                                                (status) ?
                                                    null
                                                    :
                                                    (row?.id == "V6RsLqZNQR1" || row?.id == "URjSAFQi8Yc" || row?.id == "hxfjtXMV6aS" || row?.id == "PomT3nejHKv" || row?.id == "CwSFMrVP9rE" || row?.id == "eB1XOr5TYhs") ?
                                                        <>
                                                            <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                            <TextField
                                                                type={row?.valueType}
                                                                // disabled={true}
                                                                error={obrigatory}
                                                                helperText={obrigatory && "Campo Obrigatorio"}
                                                                value={row?.value}
                                                                id={row?.id}
                                                                style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                            />
                                                        </>
                                                        :
                                                        <>

                                                            {(row?.valueType) ?
                                                                <>
                                                                    <InputLabel style={{ marginTop: 10, marginLeft: 32 }}>{row.name}</InputLabel>
                                                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                        <KeyboardDatePicker
                                                                            disableToolbar
                                                                            style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                            // variant="inline"
                                                                            format="yyyy-MM-dd"
                                                                            margin="normal"
                                                                            id={row?.id}
                                                                            error={obrigatory}
                                                                            helperText={obrigatory && "Campo Obrigatorio"}
                                                                            //    disabled={view ? view : status}
                                                                            defaultValue={row?.value}
                                                                            value={row?.value}
                                                                            onChange={date => organizar(row?.id, date, i, j)}
                                                                            KeyboardButtonProps={{
                                                                                'aria-label': 'change date',
                                                                            }}
                                                                        />
                                                                    </MuiPickersUtilsProvider>
                                                                </>
                                                                :
                                                                <>
                                                                    <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                                    <TextField
                                                                        type={row?.valueType}
                                                                        error={obrigatory}
                                                                        helperText={obrigatory && "Campo Obrigatorio"}
                                                                        // disabled={view ? view : status}
                                                                        defaultValue={row?.value}
                                                                        id={row?.id}
                                                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                        onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                                    />
                                                                </>
                                                            }


                                                        </>
                                            }
                                        </>
                                    )
                                })}
                            </Card>
                        </Grid>
                    ))}

                    <Grid item sm={12}>
                        <Button style={{ width: '140px', borderRadius: '0px' }} onClick={() => SaveForm(formData)} variant="contained" color="primary">Salvar</Button> &nbsp;
                                <Button style={{ width: '140px', borderRadius: '0px' }} component={NavLink} to="/site">Cancelar</Button>

                    </Grid>
                    <Typography variant="button" display="block" variant="h6" style={{ fontSize: 12, marginTop: 5, marginLeft: 12 }} >
                        Salvando no <b>{program}</b> em <b>{orgUnitSelected.displayName}</b>
                    </Typography>
                </Grid>
            </Paper>
            <br />
        </Container >
    )
}

export default NewSite
