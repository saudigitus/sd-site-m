import React, { useEffect } from 'react'
import { Grid, Typography } from '@material-ui/core'
// import { FolderOutline } from 'mdi-material-ui'
import Cartao from "../Components/Cartao"
// import { withTranslation } from "react-i18next"

const menus = [
    { path: '/site', label: 'Lista de Sites', description: 'Visualizar, editar e baixar dados referentes a ECHO Sites.' },
    { path: '/site-history', label: 'Histórico de Actualização', description: 'Visualizar histórico de actualização de dados referentes a ECHO Sites.' },
    // { path: '/update-site', label: 'Actualizar Sites', description: 'Descrição da funcionalidade' },
];

function Home(props) {
    useEffect(() => {
        props.setTabOrder(0)
    }, [])

    return (
        <div style={{ marginLeft: 20 }}>
            <br />
            <Grid container spacing={2}>
                {menus.map(x => <Grid style={{ margin: 10 }} item>
                    <Cartao url={x.path} title={(x.label)} description={x.description} />
                </Grid>)}
            </Grid>
        </div>
    )
}

export default Home