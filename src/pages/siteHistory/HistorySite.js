import React, { useState, useEffect } from 'react'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import Table from '../../Components/Table1'
import { format, formatDistance, formatRelative, subDays, subYears } from 'date-fns'
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import HeaderAction from '../../Components/HeaderActionHistory'

const ListaTodosEvents = {
    results: {
        resource: "events.json",
        params: ({ id, endDate, startDate, page, pageSize }) => ({
            program: "xW2D1dak6g6",
            orgUnit: [id],
            ouMode: "DESCENDANTS",
            page: page,
            pageSize: pageSize,
            order: "eventDate%3Adesc",
            totalPages: true,
            endDate: endDate,
            startDate: startDate,
            fields: 'event,attributeCategoryOptions,storedBy,code,orgUnit,lastUpdated,orgUnitName,dueDate,dataValues[dataElement,value]',
        })
    }
}

const OrgUnitsDeltalhe = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: {
            fields: 'code,ancestors[id,name]',
            paging: false,
        },
    },
}
const ListaPrograms = {
    results: {
        resource: 'programs',
        id: 'xW2D1dak6g6',
        params: {
            fields: 'id,name,programStages[programStageSections[id,name,dataElements[id,name,valueType,optionSet[id,name,options[id,name,code]]]]',
            paging: false,
        },
    },
    orgUnits: {
        resource: 'me.json',
        params: {
            fields: ['organisationUnits[id,name,code,level,created]'],
            withinUserSearchHierarchy: true,
            paging: false,
        },
    },
}
let a = 0
let dados = []
let total = []
let IndicatorDataHeaders = []
let id = ""
let ou = ""
function App(props) {
    const { loading, data } = useDataQuery(ListaPrograms)
    const engine = useDataEngine()
    const [isLoaded, setIsLoaded] = useState(false);
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(5)
    const [endDate, setendDate] = useState(new Date())
    const [startDate, setstartDate] = useState(format(subYears(new Date(), 1), 'yyyy-MM-dd'))
    useEffect(() => {
        props.setdisabled(false)
        props.setTabOrder(2)
        IndicatorDataHeaders = []
        if (!loading) {
            data.results.programStages[0].programStageSections.forEach(elem => {
                elem.dataElements.forEach(element => {
                    let dataElement = ({ key: element.id, header: element.name, width: 20, style: { numFmt: element.valueType === "INTEGER_ZERO_OR_POSITIVE" ? '#;[Red]\#' : "@" } })
                    IndicatorDataHeaders.push(dataElement)
                });
            })
            IndicatorDataHeaders.splice(0, 0, { key: "OrgUnit", header: "OrgUnit", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "orgUnitName", header: "OrgUnitName", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "distrito", header: "Distrito", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "provincia", header: "Provincia", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "dataActualizacao", header: "Data da actualizaçáo", width: 20 })

            //Caregando os dados iniciais
            listaEvents.refetch({
                id: data?.orgUnits.organisationUnits[0].id, page: page, pageSize: pageSize, startDate: format(subYears(new Date(), 1), 'yyyy-MM-dd'), endDate: format(new Date(), 'yyyy-MM-dd')
            })
        }
    }, [data])

    useEffect(() => {
        setIsLoaded(true)
    }, [])


    const listaEvents = useDataQuery(ListaTodosEvents, {
        lazy: true,
        onComplete: function name(params) {
            if (params.results.events.length == 0) {
                setIsLoaded(false)
                dados = []
            }
            total = params.results.pager
            dados = []
            a = 0
            params.results.events.forEach((element) => {
                element.lastUpdated = `${format(new Date(element.lastUpdated), 'yyyy-MM-dd HH:ii:ss')}`
                let abc = engine.query(OrgUnitsDeltalhe, { variables: { id: element.orgUnit } })
                Promise.all([abc]).then(resp => {
                    a = a + 1
                    element = ({
                        ...element,
                        code: resp[0].results?.code,
                        provincia: resp[0].results?.ancestors[1]?.name,
                        distrito: resp[0].results?.ancestors[2]?.name
                    })
                    dados.push(element)
                }).finally(() => {

                    if (a == params.results.events.length) {
                        setIsLoaded(false)
                    }

                })
            })
        },
        onError: (res) => {
            props.messageBox(res)
        }
    })

    const handleEvents = (ids, startDat, endDat) => {
        setstartDate(startDat)
        setendDate(endDat)
        id = ids
        if ((ids == "" && endDat) || (ids == "" && startDat)) {
            setIsLoaded(true)
            listaEvents.refetch({ id: ids, page: page, pageSize: pageSize, startDate: format(new Date(startDat), 'yyyy-MM-dd'), endDate: format(new Date(endDat), 'yyyy-MM-dd') })
        } else {
            setIsLoaded(true)
            listaEvents.refetch({ id: ids, page: page, pageSize: pageSize, startDate: format(new Date(startDat), 'yyyy-MM-dd'), endDate: format(new Date(endDat), 'yyyy-MM-dd') })
        }

    }
    const handleChangePage = (pages) => {
        setPage(pages);
        if (id == "") {
            setIsLoaded(true)
            listaEvents.refetch({ id: ou, page: pages, pageSize: pageSize, startDate: format(subYears(new Date(), 1), 'yyyy-MM-dd'), endDate: format(new Date(), 'yyyy-MM-dd') })
        } else {
            setIsLoaded(true)
            listaEvents.refetch({ id: id, page: pages, pageSize: pageSize, startDate: format(new Date(startDate), 'yyyy-MM-dd'), endDate: format(new Date(endDate), 'yyyy-MM-dd') })
        }
    }


    const handleChangeRowsPerPage = (size) => {
        setPage(1);
        setpageSize(size)
        if (id == "") {
            setIsLoaded(true)
            listaEvents.refetch({ id: ou, page: page, pageSize: size, startDate: format(subYears(new Date(), 1), 'yyyy-MM-dd'), endDate: format(new Date(), 'yyyy-MM-dd') })
        } else {
            setIsLoaded(true)
            listaEvents.refetch({ id: id, page: page, pageSize: size, startDate: format(new Date(startDate), 'yyyy-MM-dd'), endDate: format(new Date(endDate), 'yyyy-MM-dd') })
        }
    };

    const [state, setState] = React.useState({
        columns: [
            { title: 'Provincia', field: 'provincia' },
            { title: 'Distrito', field: 'distrito' },
            { title: 'Unidade Sanitária', field: 'orgUnitName' },
            { title: 'Código', field: 'code' },
            { title: 'Última atualização', field: 'lastUpdated' },
            { title: 'Actualizado por', field: 'storedBy' },

        ]
    });

    if (loading) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }

    return (
        <Grid container direction="column" >
            <HeaderAction greetHandler={handleEvents} />
            <Grid style={{ marginLeft: 30, marginRight: 30, marginBottom: 50, marginTop: 20 }}>
                <Paper elevation={3}>
                    {isLoaded && <LinearProgress />}
                    <Table
                        title={"Histórico de Actualização"}
                        columns={state.columns}
                        data={dados}
                        totalRow={total?.total ? total?.total : 0}
                        pages={page}
                        changePage={handleChangePage}
                        changeRowsPerPage={handleChangeRowsPerPage}
                        size={pageSize}
                        headers={IndicatorDataHeaders}
                        id={id ? id : ou}
                        endDate={endDate}
                        startDate={startDate}
                        back={true}
                        status={1}
                    />
                </Paper>

            </Grid>

        </Grid>
    )
}

export default App
