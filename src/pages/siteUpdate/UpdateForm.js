import 'date-fns';
import React, { useEffect, useState } from 'react';
import { NavLink, withRouter, useHistory } from "react-router-dom";
import {
    Box, Paper, Container, TextField, InputLabel, Snackbar,
    Grid, List, ListItem, Divider, CircularProgress,
    Dialog, DialogContent, DialogTitle, DialogActions,
    ListItemSecondaryAction, IconButton, InputAdornment,
    ListItemText, Typography, Button, MenuItem, Card, Select
} from "@material-ui/core";
import ArrowLeftIcon from '@material-ui/icons/ArrowBack';
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import { useParams } from 'react-router-dom'
import { format, formatDistance, formatRelative, subDays, getMonth, getDay } from 'date-fns'
import { useDataMutation } from '@dhis2/app-runtime'

import DateFnsUtils from '@date-io/date-fns';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider
} from '@material-ui/pickers';

const ListaPrograms = {
    results: {
        resource: 'programs',
        id: "xW2D1dak6g6",
        params: {
            fields: "id,name,programStages[programStageSections[id,name,dataElements[id,name,valueType,optionSet[id,name,options[id,name,code]]]]",
            paging: false,
        },
    },
}

const ListaTodosEvents = {
    results: {
        resource: "events",
        id: ({ id }) => id,
    }
}

const TxCurrAll = {
    results: {
        resource: "analytics",
        params: ({ orgUnit, period }) => ({
            dimension: [
                "dx:H86N56K3xJd",
                "pe:" + period,
                "ou:" + orgUnit
            ],
            // filter: "ou:" + orgUnit,
            displayProperty: "NAME",
            skipMeta: true,
            includeNumDen: true,
            paging: false,
        }),
    }
}

const ListaUserOrgUnits = {
    results: {
        resource: 'me.json',
        params: {
            fields: ['organisationUnits[id,name,code,level,created]'],
            withinUserSearchHierarchy: true,
            paging: false,
        },
    },
}

const SaveEvent = {
    resource: 'events',
    type: "create",
    data: ({ formEvent }) => formEvent,
}

const fieldsMapping = [
    {
        mainDateElement: "KLWeSxlzFfk", masterDataElement: "pqkZ5eRXQeU", value: "1"
    },
    {
        mainDateElement: "Ys5yIsJUJD4", masterDataElement: "pqkZ5eRXQeU", value: "0"
    },
    {
        mainDateElement: "vDkQtqhzYbT", masterDataElement: "uM6X63gpZf9", value: "0"
    },
    {
        mainDateElement: "AbHxI6ztCbw", masterDataElement: "QBCGkrVdrCN", value: "0"
    },
    {
        mainDateElement: "HU05vCWp17q", masterDataElement: "EbpxmdSa192", value: "0"
    },
    {
        mainDateElement: "eOm4bNyTAm3", masterDataElement: "HcsdMnMtOur", value: "0"
    },
    {
        mainDateElement: "TPAj0hNZZ2g", masterDataElement: "VPlQWsj7ZFq", value: "1"
    },
    {
        mainDateElement: "kC3JUSy1tzN", masterDataElement: "D9Ay6QmQVqM", value: "1"
    },
    {
        mainDateElement: "sO4F0BizoYz", masterDataElement: "M5FFNTKXSnm", value: "1"
    },
    {
        mainDateElement: "eOm4bNyTAm3", masterDataElement: "DYl0bZseIS3", value: "0"
    },

]


let dados = []
let formData = []
// let newValueCurr = ""
let dadosGeral = []
let dadosAnexo = []
let orgUnit = ""
let programName = ""
let orgUnitId = ""
let programId = ""
let eventDate = ""
let view = true
let a = 0
let formFields = []
let period = ""
function UserForm(props) {
    const engine = useDataEngine()
    const history = useHistory();
    const [state, setstate] = useState(true)
    const [state1, setstate1] = useState(true)
    const [TodosDados, setTodosDados] = useState([])
    const [startDate, setstartDate] = useState(format(new Date(), 'yyyy-MM-dd'))
    const [view, setview] = useState(true)
    const [newValueCurr, setnewValueCurr] = useState("")
    // const [formFields, setformFields] = useState([])

    let slug = useParams();


    let userOrgUnit = useDataQuery(ListaUserOrgUnits)


    const [mutate, response] = useDataMutation(SaveEvent, {
        onError: function (params) {
            setstate(true)
        },
        onComplete: function (params) {
            props.messageBox('Actualizado com sucesso!');
            props.setTabOrder(1)
            history.replace("/site");
        }
    })

    if (response.error?.details && state) {
        props.messageBox(response.error?.details?.response.importSummaries[0].description)
        setstate(false)
    }



    useEffect(() => {
        dadosGeral = []
        dadosAnexo = []
        a = 0
    }, [])

    const checkId = (elem, section) => {

        for (let fieldMapping of fieldsMapping) {
            if (elem.id == fieldMapping.mainDateElement) {
                for (let sectionDataElement of section.dataElements) {
                    if (fieldMapping.masterDataElement == sectionDataElement.id) {
                        if (fieldMapping.value != sectionDataElement.value) {
                            return true
                        }
                    }
                }
            }
        }

        return false
    }




    const checkvValue = (element, dadosEvent, i, a) => {
        dados[i].dataElements[a] = { ...dados[i].dataElements[a], value: null }
        dadosEvent.forEach(elem => {
            if (elem.dataElement == dados[i].dataElements[a].id) {
                dados[i].dataElements[a] = { ...dados[i].dataElements[a], value: elem.value }
            }
        });
        return a = 0
    }

    useEffect(() => {
        props.setdisabled(true)
        // console.log(slug.view)
        setview(slug.view ? slug.view : false)

    }, [])

    useEffect(() => {
        getallData()
    }, [])

    async function getallData() {
        let dadosEvent = []
        const data = engine.query(ListaPrograms)
        let programs = engine.query(ListaTodosEvents, { variables: { id: slug.id } })
        Promise.all([data, programs]).then((resp) => {
            dados = resp[0].results.programStages[0].programStageSections
            // console.log(data?.results.programStages[0].programStageSections)
            // console.log(resp[1].results.dataValues)
            dadosEvent = resp[1].results?.dataValues
            // console.log(resp[1].results)
            dadosAnexo = resp[1].results.events
            orgUnit = resp[1].results.orgUnitName
            orgUnitId = resp[1].results.orgUnit
            eventDate = resp[1].results.eventDate
            programName = resp[0].results.name
            programId = resp[0].results.id


            dados?.forEach((elemn, i) => {
                a = -1
                elemn.dataElements?.forEach((element) => {
                    a = a + 1
                    //   console.log(element,i,a)
                    let elem = checkvValue(element, dadosEvent, i, a)
                    // dadosGeral.push(elem)
                });
                a = -1
            });
            verifyLastMonth()
            setTodosDados(dados)

        }).finally(() => {
            // setstate(false)
        })

    }


    async function verifyLastMonth() {
        let thisMonth = format(new Date(eventDate), "MM")
        let thisYear = format(new Date(eventDate), "yyyy")
        // console.log(thisYear)
        let trimestres = [
            ["01", "02", "03"],
            ["04", "05", "06"],
            ["07", "08", "09"],
            ["10", "11", "12"]
        ]
        // let period = ""
        trimestres.forEach((element, i) => {
            element.forEach((elem, y) => {
                if (elem === thisMonth) {
                    period = i > 0 ? thisYear + "" + trimestres[i > 0 ? i - 1 : 3][2] : thisYear - 1 + "" + trimestres[i > 0 ? i - 1 : 3][2]
                }
            });
        });
        let txCur = engine.query(TxCurrAll, {
            variables: { orgUnit: orgUnitId, period: period }
        })
        let idRow
        Promise.all([txCur]).then(resp => {
            // console.log(resp[0].results?.headers.filter(element => element.name=="value"))

            for (let index = 0; index < resp[0].results?.headers.length; index++) {
                if (resp[0].results?.headers[index].name == "value") {
                    idRow = index
                }

            }
            if (resp[0].results?.rows[0]) {
                setnewValueCurr(resp[0].results?.rows[0][idRow])
            }
            // newValueCurr = resp[0].results?.rows[0][idRow]
            setstate1(false)
        }).finally(() => {

        })
    }


    const organizar = (id, value, i, j) => {
        let newTodosDados = [...TodosDados]

        // console.log(clearFields(newTodosDados[i].dataElements[j].id, newTodosDados[i].dataElements))

        // if (clearFields(value, newTodosDados[i].dataElements[j].id, newTodosDados[i].dataElements)) {
        //     //  console.log(newTodosDados[i].dataElements[clearFields(value, newTodosDados[i].dataElements[j].id, newTodosDados[i].dataElements)])
        //     newTodosDados[i].dataElements[clearFields(value, newTodosDados[i].dataElements[j].id, newTodosDados[i].dataElements)].value = ""
        // }
        newTodosDados[i].dataElements[j].value = value
        // console.log(dados)
        setTodosDados(newTodosDados)
        //  console.log(value,i,j)
        dados[i].dataElements[j].value = value
        // setTodosDados(dados)
    }

    const SaveForm = async (formData) => {
        // console.log(formFields)
        let arrayForm = []
        let arraySave = []

        dados.forEach((elemen, i) => {
            dados[i].dataElements.forEach(element => {
                if(element.id==='eB1XOr5TYhs'){
                arrayForm = ({
                    ...arrayForm, "dataElement": element.id,
                    "value":  parseInt(newValueCurr, 10)
                })
                }else{
                arrayForm = ({
                    ...arrayForm, "dataElement": element.id,
                    "value": element.value
                })
                }
               
                for (const fields of formFields) {
                    if (element.id === fields) {
                        arraySave.push(arrayForm)
                    }
                }

            });
        });


        let form = ({
            "program": programId,
            "orgUnit": orgUnitId,
            "eventDate": startDate,
            "dataValues": arraySave
        })

        await mutate({
            formEvent: form
        })

        // if (error == undefined) {
        //     alert("salvo com sucesso")
        // }
    }

    const EditEvents = async (formData) => {
        let arrayForm = []
        let arraySave = []

        dados.forEach((elemen, i) => {
            dados[i].dataElements.forEach(element => {
                arrayForm = ({
                    ...arrayForm, "dataElement": element.id,
                    "value": element.value
                })
                for (const fields of formFields) {
                    if (element.id === fields) {
                        arraySave.push(arrayForm)
                    }
                }

            });
        });


        let form = ({
            "event": slug.id,
            "program": programId,
            "orgUnit": orgUnitId,
            "eventDate": startDate,
            "dataValues": arraySave
        })

        await mutate({
            formEvent: form
        })

        // if (response.error) {
        //     alert(response.error?.details?.response.importSummaries[0].description)
        // }
        // if (error == undefined) {
        //     alert("salvo com sucesso")
        // }
    }


    if (state1) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }

    // console.log(newValueCurr)
    const checkForm = (status, row) => {
        if (!status) {
            formFields.push(row.id)
        }
    }


    formFields = []
    return (
        <Container maxWidth="xl" style={{ margin: 10 }}>
            <br />
            <Box display="flex">
                <Box>
                    <IconButton component={NavLink} to="/site">
                        <ArrowLeftIcon />
                    </IconButton>
                </Box>
                {(slug.edit) ?
                    <Box style={{ paddingTop: '7px', marginRight: 50 }} flexGrow={1} component={Typography} variant="h5">Actualizar o Assessment</Box>
                    :
                    (slug.view) ?
                        view ?
                            <Box style={{ paddingTop: '7px', marginRight: 50 }} flexGrow={1} component={Typography} variant="h5">Visualizar o Assessment</Box>
                            :
                            null
                        // <Box style={{ paddingTop: '7px', marginRight: 50 }} flexGrow={1} component={Typography} variant="h5">Editar Evento</Box>
                        :
                        <Box style={{ paddingTop: '7px', marginRight: 50 }} flexGrow={1} component={Typography} variant="h5">Adicionar Assessment</Box>
                }

            </Box>

            <br />
            <Card elevation={2} style={{ borderRadius: 0, marginTop: 20 }}>
                <Grid item xs={12} sm container style={{ position: 'relative', margin: 40 }}>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs style={{ width: 330, height: 80, borderRadius: 0, textAlign: 'center', marginRight: 70 }}>
                            {/* <Card elevation={2} style={{ width: 230, height: 80, borderRadius: 0, textAlign: 'center' }}> */}
                            <Typography variant="button" display="block" style={{ paddingTop: '8px' }} variant="h6">
                                Unidade Organizacional
                                </Typography>
                            <Typography style={{ minHeight: 120, padding: '14px' }} variant="body2"
                                color="textSecondary" component="p">
                                <b>{orgUnit}</b>
                            </Typography>
                            {/* </Card> */}
                        </Grid>
                    </Grid>
                    <Grid item style={{ width: 230, height: 80, borderRadius: 0, textAlign: 'center', marginRight: 70 }}>
                        {/* <Card elevation={2} style={{ width: 230, height: 80, borderRadius: 0, textAlign: 'center', marginRight: 70 }}> */}
                        <Typography variant="button" display="block" style={{ paddingTop: '8px' }} variant="h6">
                            Programa
                                </Typography>
                        <Typography style={{ minHeight: 120, padding: '14px' }} variant="body2"
                            color="textSecondary" component="p">
                            <b>{programName}</b>
                        </Typography>
                        {/* </Card> */}
                    </Grid>
                </Grid>
            </Card>
            <br />
            <Paper elevation={3} style={{ padding: '28px 70px 56px' }}>
                {view ?
                    <Grid item xs={12} sm container style={{ position: 'relative' }}>
                        <Grid item xs container direction="column" spacing={2}>
                        </Grid>
                        {/* <Grid item style={{ marginRight: 10 }}>
                            <Button style={{ width: '200px', borderRadius: '0px' }} variant="contained" onClick={() => setview(!view)}>Editar Evento</Button>
                        </Grid> */}
                    </Grid>
                    :
                    null
                }
                <Grid container spacing={2}>
                    <Grid item sm={12} style={{ margin: 10, marginTop: 20 }}>
                        <InputLabel style={{ marginTop: 20, marginBottom: 20 }}><b>{"INFORMAÇÁO BÁSICA"}</b></InputLabel>
                        <Card elevation={2} style={{ borderRadius: 0, marginTop: 20 }}>
                            <InputLabel style={{ marginTop: 10, marginLeft: 32 }}>{"Event Date"}</InputLabel>
                            {view ?
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                        // // variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        id="date-picker-inline"
                                        // label={""}
                                        disabled={view ? view : false}
                                        value={eventDate}
                                        //  onChange={(date) => setstartDate(date)}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                                :
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                        // // variant="inline"
                                        format="yyyy-MM-dd"
                                        margin="normal"
                                        id="date-picker-inline"
                                        // label={""}
                                        value={startDate}
                                        onChange={(date) => setstartDate(date)}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            }

                        </Card>
                    </Grid>

                    {TodosDados?.map((root, i) => (
                        <Grid item sm={12} style={{ margin: 10 }}>
                            <InputLabel style={{ marginTop: 20, marginBottom: 20 }}><b>{root?.name}</b></InputLabel>
                            <Card elevation={2} style={{ borderRadius: 0 }}>
                                {TodosDados[i].dataElements.map((row, j) => {

                                    let status = checkId(row, TodosDados[i])
                                    checkForm(status, row)

                                    return (
                                        <>
                                            {(row?.optionSet) ?
                                                <>
                                                    <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                    <Select
                                                        // select
                                                        defaultValue={row?.value}
                                                        id={row?.id}
                                                        disabled={view ? view : status}
                                                        //   value={row?.value}
                                                        // style={{ width: "96%" }}
                                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                        onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                    >
                                                        {row?.optionSet.options.map((option) => (
                                                            <MenuItem key={option.code} value={option.code}>
                                                                {option.name}
                                                            </MenuItem>
                                                        ))}
                                                    </Select>
                                                </>
                                                :
                                                (status) ?
                                                    null
                                                    :
                                                    // (row?.id == "V6RsLqZNQR1" || row?.id == "URjSAFQi8Yc" || row?.id == "hxfjtXMV6aS" || row?.id == "PomT3nejHKv" || row?.id == "CwSFMrVP9rE" ) ?
                                                    //     <>
                                                    //         <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                    //         <TextField
                                                    //             type={row?.valueType}
                                                    //             disabled={slug.view ? view : true}
                                                    //             defaultValue={row?.value}
                                                    //             id={row?.id}
                                                    //             style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                    //             onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                    //         />
                                                    //     </>
                                                    // :
                                                    <>

                                                        {(row?.valueType === "DATE") ?
                                                            <>
                                                                <InputLabel style={{ marginTop: 10, marginLeft: 32 }}>{row.name}</InputLabel>
                                                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                    <KeyboardDatePicker
                                                                        disableToolbar
                                                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                        // variant="inline"
                                                                        format="yyyy-MM-dd"
                                                                        margin="normal"
                                                                        id={row?.id}
                                                                        disabled={view ? view : status}
                                                                        defaultValue={row?.value}
                                                                        value={row?.value}
                                                                        onChange={date => organizar(row?.id, date, i, j)}
                                                                        KeyboardButtonProps={{
                                                                            'aria-label': 'change date',
                                                                        }}
                                                                    />
                                                                </MuiPickersUtilsProvider>
                                                            </>
                                                            :
                                                            (row?.valueType === "INTEGER_ZERO_OR_POSITIVE") ?
                                                                <>


                                                                    {(row?.id === "eB1XOr5TYhs" && view === false) ?
                                                                        <Grid style={{ display: "flex" }}>
                                                                            <div style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}>
                                                                                <InputLabel style={{ marginTop: 10, marginBottom: 10 }}>{row.name}</InputLabel>
                                                                                <TextField
                                                                                    type={"number"}
                                                                                    disabled={true}
                                                                                    defaultValue={row?.value}
                                                                                    // value={row?.id === "eB1XOr5TYhs" ? (row?.value === null || "" ? newValueCurr : row?.value) : row?.value}
                                                                                    value={newValueCurr}
                                                                                    id={row?.id}
                                                                                    style={{ width: "96%", marginBottom: 10 }}
                                                                                    onChange={e => organizar(row?.id, newValueCurr < 0 ? 0 : newValueCurr, i, j)}
                                                                                />
                                                                            </div>
                                                                            <div style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}>
                                                                                <InputLabel style={{ marginTop: 10, marginBottom: 10 }}>{"Período Referente"}</InputLabel>
                                                                                <TextField
                                                                                    type={"number"}
                                                                                    disabled={true}
                                                                                    defaultValue={row?.value}
                                                                                    // value={row?.id === "eB1XOr5TYhs" ? (row?.value === null || "" ? newValueCurr : row?.value) : row?.value}
                                                                                    value={period}
                                                                                    id={row?.id}
                                                                                    style={{ width: "96%", marginBottom: 10 }}
                                                                                    onChange={e => organizar(row?.id, e.target.value < 0 ? 0 : e.target.value, i, j)}
                                                                                />
                                                                            </div>
                                                                        </Grid>

                                                                        :
                                                                        <>
                                                                            <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                                            <TextField
                                                                                type={"number"}
                                                                                disabled={view ? view : status}
                                                                                defaultValue={row?.value}
                                                                                // value={row?.id === "eB1XOr5TYhs" ? (row?.value === null || "" ? newValueCurr : row?.value) : row?.value}
                                                                                value={row?.value}
                                                                                id={row?.id}
                                                                                style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                                onChange={e => organizar(row?.id, e.target.value < 0 ? 0 : e.target.value, i, j)}
                                                                            />
                                                                        </>
                                                                    }

                                                                </>
                                                                :
                                                                <>

                                                                    <InputLabel style={{ marginTop: 10, marginLeft: 32, marginBottom: 10 }}>{row.name}</InputLabel>
                                                                    <TextField
                                                                        type={row?.valueType}
                                                                        disabled={view ? view : status}
                                                                        defaultValue={row?.value}
                                                                        id={row?.id}
                                                                        style={{ width: "96%", marginLeft: 32, marginBottom: 10 }}
                                                                        onChange={e => organizar(row?.id, e.target.value, i, j)}
                                                                    />
                                                                </>
                                                        }

                                                    </>
                                            }
                                        </>
                                    )
                                })}
                            </Card>
                        </Grid>
                    ))}
                    <Grid item sm={12}>
                        {
                            (slug.edit) ?
                                <>
                                    <Button style={{ width: '140px', borderRadius: '0px' }} onClick={() => SaveForm(formData)} variant="contained" color="primary">Actualizar</Button> &nbsp;
                                        <Button style={{ width: '140px', borderRadius: '0px' }} component={NavLink} to="/site">Cancelar</Button>
                                </>
                                :
                                (slug.view) ?
                                    (view) ?
                                        null
                                        :
                                        <>
                                            <Button style={{ width: '140px', borderRadius: '0px' }} onClick={() => EditEvents(formData)} variant="contained" color="primary">Salvar</Button> &nbsp;
                                <Button style={{ width: '140px', borderRadius: '0px' }} component={NavLink} to="/site">Cancelar</Button>
                                        </>
                                    :
                                    <>
                                        <Button style={{ width: '140px', borderRadius: '0px' }} onClick={() => SaveForm(formData)} variant="contained" color="primary">Salvar</Button> &nbsp;
                                <Button style={{ width: '140px', borderRadius: '0px' }} component={NavLink} to="/site">Cancelar</Button>
                                    </>
                        }
                    </Grid>
                </Grid>
            </Paper>
            <br />
        </Container >
    )
}

export default withRouter(UserForm);