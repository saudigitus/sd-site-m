import React, { useState, useEffect } from 'react'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import Table from '../../Components/Table'
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import HeaderAction from '../../Components/HeaderAction'
// import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialIcon from '@material-ui/icons/Add';
import SpeedDial from '@material-ui/lab/SpeedDial';
import { Link, NavLink } from "react-router-dom";
import { useConfig } from '@dhis2/app-runtime'

const ListaTodosEvents = {
    results: {
        resource: "events.json",
        params: ({ id, endDate, startDate, page, pageSize }) => ({
            program: "xW2D1dak6g6",
            orgUnit: [id],
            ouMode: "DESCENDANTS",
            // page: page,
            // pageSize: pageSize,
            order: "eventDate%3Adesc",
            totalPages: true,
            paging: false,
            endDate: endDate,
            startDate: startDate,
            fields: 'event,attributeCategoryOptions,storedBy,code,orgUnit,lastUpdated,orgUnitName,dataValues[dataElement,value]'
        })
    }
}

const OrgUnitsDeltalhe = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: {
            fields: 'code,ancestors[id,name]',
            paging: false,
        },
    },
}
const ListaPrograms = {
    results: {
        resource: 'programs',
        id: 'xW2D1dak6g6',
        params: {
            fields: 'id,name,programStages[programStageSections[id,name,dataElements[id,name,valueType,optionSet[id,name,options[id,name,code]]]]',
            paging: false,
        },
    },
    orgUnits: {
        resource: 'me.json',
        params: {
            fields: ['organisationUnits[id,name,code,level,created]'],
            withinUserSearchHierarchy: true,
            paging: false,
        },
    },

}

let a = 0
let b = 1
let dados = []
let total = []
let IndicatorDataHeaders = []
let id = ""
let ou = ""
function App(props) {
    const { loading, data } = useDataQuery(ListaPrograms)
    const engine = useDataEngine()
    const [isLoaded, setIsLoaded] = useState(false);
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(10)
    const [orgUnit, setorgUnit] = useState("")
    const { baseUrl, apiVersion } = useConfig()

    useEffect(() => {
        setIsLoaded(true)
    }, [])

    useEffect(() => {
        props.setTabOrder(1)
        props.setdisabled(false)
        IndicatorDataHeaders = []
        if (!loading) {
            //  console.log(data.results.programStages[0].programStageSections)
            data.results.programStages[0].programStageSections.forEach(elem => {
                elem.dataElements.forEach(element => {
                    let dataElement = ({
                        key: element.id, header: element.name, width: 20,
                        style: { numFmt: element.valueType === "INTEGER_ZERO_OR_POSITIVE" ? '#;[Red]\#' : "@" }
                    })
                    IndicatorDataHeaders.push(dataElement)
                });
            })
            IndicatorDataHeaders.splice(0, 0, { key: "OrgUnit", header: "OrgUnit", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "orgUnitName", header: "OrgUnitName", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "distrito", header: "Distrito", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "provincia", header: "Provincia", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "dataActualizacao", header: "Data da actualizaçáo", width: 20, type: "TEXT" })
            
            //Carregando dados inciais
            listaEvents.refetch({ id: data?.orgUnits.organisationUnits[0].id })
        }
    }, [data])

    const listaEvents = useDataQuery(ListaTodosEvents, {
        lazy: true,
        onComplete: function name(params) {
            if (params.results.events.length == 0) {
                setIsLoaded(false)
                dados = []
            }
            total = params.results.pager
            dados = []
            b = 1
            for (let index = 0; index < params.results.events.length; index++) {
                b = 1
                for (let i = params.results.events.length; i > index; i--) {
                    if ((params.results.events[index]?.orgUnitName === params.results.events[i]?.orgUnitName)) {
                        b = b + 1
                        params.results.events[index].attributeCategoryOptions = b
                        params.results.events.splice(i, 1)
                    } else {
                        params.results.events[index].attributeCategoryOptions = b
                    }
                }
            }
            a = 0
            params.results.events.forEach((element) => {
                element.lastUpdated = `${format(new Date(element.lastUpdated), 'yyyy-MM-dd HH:ii:ss')}`
                let abc = engine.query(OrgUnitsDeltalhe, { variables: { id: element.orgUnit } })
                Promise.all([abc]).then(resp => {
                    a = a + 1
                    element = ({
                        ...element,
                        code: resp[0].results?.code,
                        provincia: resp[0].results?.ancestors[1]?.name,
                        distrito: resp[0].results?.ancestors[2]?.name,
                    })
                    dados.push(element)
                }).finally(() => {

                    if (a == params.results.events.length) {

                        setIsLoaded(false)
                    }

                })
            })
        },
        onError: (res) => {
            props.messageBox(res)
        }
    })

    

    const handleEvents = (ids, startDat, endDat) => {
        console.log(format(new Date(startDat), 'yyyy-MM-dd'), format(new Date(endDat), 'yyyy-MM-dd'))

        id = ids
        if ((ids == "" && endDat) || (ids == "" && startDat)) {
            setIsLoaded(true)
            listaEvents.refetch({ id: ids, page: page, pageSize: pageSize, startDate: format(new Date(startDat), 'yyyy-MM-dd'), endDate: format(new Date(endDat), 'yyyy-MM-dd') })
        } else {
            setIsLoaded(true)
            listaEvents.refetch({ id: ids, page: page, pageSize: pageSize, startDate: format(new Date(startDat), 'yyyy-MM-dd'), endDate: format(new Date(endDat), 'yyyy-MM-dd') })
        }

    }


    const [state, setState] = React.useState({
        columns: [
            { title: 'Provincia', field: 'provincia' },
            { title: 'Distrito', field: 'distrito' },
            { title: 'Unidade Sanitária', field: 'orgUnitName' },
            { title: 'Código', field: 'code' },
            { title: 'Última atualização', field: 'lastUpdated' },
            { title: 'Assessment', field: 'attributeCategoryOptions' },
            { title: 'Actualizado por', field: 'storedBy' },
        ]
    });

    if (loading) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }
    return (
        <Grid container direction="column" >
            {/* <Link to={`/new-event`}>
                <SpeedDial
                    ariaLabel="SpeedDial example"
                    icon={<SpeedDialIcon />}
                    style={{ position: "fixed", bottom: 25, right: 35 }}
                />
            </Link> */}

            <HeaderAction greetHandler={handleEvents} />
            <Grid style={{ marginLeft: 30, marginRight: 30, marginBottom: 50, marginTop: 20 }}>
                <Paper elevation={3}>
                    {isLoaded && <LinearProgress />}
                    <Table
                        title={"Lista de Sites"}
                        columns={state.columns}
                        data={dados}
                        totalRow={total?.total ? total?.total : 0}
                        pages={page}
                        size={pageSize}
                        headers={IndicatorDataHeaders}
                        id={id ? id : ou}
                        back={true}
                        status={0}
                        isLoaded={isLoaded}
                    />
                </Paper>

            </Grid>

        </Grid>
    )
}

export default App
