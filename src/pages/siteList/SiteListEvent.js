import React, { useState, useEffect } from 'react'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import Table from '../../Components/Table1'
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import ArrowLeftIcon from '@material-ui/icons/ArrowBack';
import HeaderAction from '../../Components/HeaderAction'
import { Link, NavLink, useParams } from "react-router-dom";

const ListaTodosEvents = {
    results: {
        resource: "events.json",
        params: ({ id, program, page, pageSize }) => ({
            program: "xW2D1dak6g6",
            orgUnit: [id],
            ouMode: "DESCENDANTS",
            page: page,
            pageSize: pageSize,
            order: "eventDate%3Adesc",
            totalPages: true,
            fields: 'event,attributeCategoryOptions,storedBy,code,orgUnit,lastUpdated,orgUnitName,dueDate,dataValues[dataElement,value]',
        })
    }
}

const OrgUnitsDeltalhe = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: {
            fields: 'code,ancestors[id,name]',
            paging: false,
        },
    },
}
const ListaPrograms = {
    results: {
        resource: 'programs',
        id: 'xW2D1dak6g6',
        params: {
            fields: 'id,name,programStages[programStageSections[id,name,dataElements[id,name,valueType,optionSet[id,name,options[id,name,code]]]]',
            paging: false,
        },
    },
}
let a = 0
let b = 1
let dados1 = []
let total1 = []
let IndicatorDataHeaders = []
function App(props) {
    const engine = useDataEngine()
    const [isLoaded, setIsLoaded] = useState(true);
    const [page, setPage] = useState(1);
    const [pageSize, setpageSize] = useState(5)
    const [orgUnit, setorgUnit] = useState("")
    const [dados, setdados] = useState([])
    const [total, settotal] = useState(0)

    let slug = useParams();




    useEffect(() => {
        props.setdisabled(true)
        props.setTabOrder(props.tabOrder)
        IndicatorDataHeaders = []
        const data = engine.query(ListaPrograms)
        let programs = engine.query(ListaTodosEvents, { variables: { id: slug.id } })
        Promise.all([data, programs]).then((resp) => {

            resp[0].results.programStages[0].programStageSections.forEach(elem => {
                elem.dataElements.forEach(element => {

                    let dataElement = ({ key: element.id, header: element.name, width: 20, style: { numFmt: element.valueType === "INTEGER_ZERO_OR_POSITIVE" ? '#;[Red]\#' : "@" } })
                    IndicatorDataHeaders.push(dataElement)
                });
            })
            IndicatorDataHeaders.splice(0, 0, { key: "OrgUnit", header: "OrgUnit", width: 20, type: "TEXT" })
            IndicatorDataHeaders.splice(0, 0, { key: "orgUnitName", header: "OrgUnitName", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "distrito", header: "Distrito", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "provincia", header: "Provincia", width: 20 })
            IndicatorDataHeaders.splice(0, 0, { key: "dataActualizacao", header: "Data da actualizaçáo", width: 20 })

            total1 = resp[1].results.pager.total
            dados1 = []
            settotal(total1)
            console.log(resp[1].results.events)
            a = 0
            resp[1].results.events.forEach((element) => {
                element.lastUpdated = `${format(new Date(element.lastUpdated), 'yyyy-MM-dd HH:ii:ss')}`
                // element.created = `${format(new Date(element.created), 'yyyy-MM-dd HH:ii:ss')}`
                // element.dueDate = `${format(new Date(element.dueDate), 'yyyy-MM-dd HH:ii:ss')}`
                let abc = engine.query(OrgUnitsDeltalhe, { variables: { id: element.orgUnit } })
                Promise.all([abc]).then(resp => {
                    a = a + 1
                    element = ({
                        ...element,
                        code: resp[0].results?.code,
                        provincia: resp[0].results?.ancestors[1]?.name,
                        distrito: resp[0].results?.ancestors[2]?.name,
                    })
                    dados1.push(element)

                }).finally(() => {

                    if (a == resp[1].results.events.length) {

                        setdados(dados1)
                        setIsLoaded(false)
                    }

                })
            })


        }).finally(() => {
        })


    }, [])

    const [id, setid] = useState("")

    const [state, setState] = React.useState({
        columns: [
            { title: 'Provincia', field: 'provincia' },
            { title: 'Distrito', field: 'distrito' },
            { title: 'Unidade Sanitária', field: 'orgUnitName' },
            { title: 'Código', field: 'code' },
            { title: 'Última atualização', field: 'lastUpdated' },
            { title: 'Actualizado por', field: 'storedBy' },
        ]
    });

    if (isLoaded) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }

    return (
        <Grid container direction="column" >
            <Grid >
                {(props.tabOrder === 1) ?
                    <IconButton component={NavLink} to="/site" style={{ marginLeft: 10 }} color="primary">
                        <ArrowLeftIcon />
                    </IconButton>
                    :
                    <IconButton component={NavLink} to="/site-history" style={{ marginLeft: 10 }} color="primary">
                        <ArrowLeftIcon />
                    </IconButton>
                }


            </Grid>
            <Grid style={{ marginLeft: 50, marginRight: 50, marginBottom: 50, marginTop: 20 }}>
                <Paper elevation={3}>

                    {isLoaded && <LinearProgress />}
                    <Table
                        title={"Histórico de Actualização"}
                        columns={state.columns}
                        data={dados}
                        totalRow={total}
                        pages={page}
                        // changePage={handleChangePage}
                        // changeRowsPerPage={handleChangeRowsPerPage}
                        size={pageSize}
                        headers={IndicatorDataHeaders}
                        id={slug.id}
                        status={2}
                    />
                </Paper>

            </Grid>

        </Grid>
    )
}

export default App
