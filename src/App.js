import React, { useState } from 'react'
import { App as D2UIApp, mui3theme as dhis2theme } from '@dhis2/d2-ui-core';
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
// import Indicator from './Indicator/MyApp'
import {
    HashRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import './App.css'
import Home from './pages/Home'
import Site from './pages/siteList/SiteList'
import Tab from './Components/Tab'
import UpdateSite from './pages/siteUpdate/UpdateForm'
import HistorySite from './pages/siteHistory/HistorySite'
import Historico from './pages/siteList/SiteListEvent'
import { IconButton, Box, Snackbar } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import NewEvent from "./pages/siteCreate/NewSite"

// const UserOrgUnits = {
//     results: {
//         resource: 'me.json',
//         params: {
//             fields: ['organisationUnits[id,name,code,level,created]'],
//             withinUserSearchHierarchy: true,
//             paging: false,
//         },
//     },
// }


export default function App() {
    // const { loading, data } = useDataQuery(UserOrgUnits)
    const [tabOrder, setTabOrder] = useState(0);
    const [showSnackbar, setShowSnackbar] = useState(false);
    const [snackbarmessage, setSnackbarmessage] = useState('');
    const [view, setview] = useState(true)
    const [disabled, setdisabled] = useState(false)
    // console.log(tabOrder)
    function popSnackbar(message) {
        setSnackbarmessage(message);
        setShowSnackbar(true);
    }
    return (
        <>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={showSnackbar}
                autoHideDuration={6000}
                onClose={() => setShowSnackbar(false)}
                message={snackbarmessage}
                action={
                    <IconButton size="small" aria-label="close" color="inherit" onClick={() => setShowSnackbar(false)}>
                        <Close fontSize="small" />
                    </IconButton>
                } />
            <Router >
                <Box />
                <Tab tabOrder={tabOrder} disabled={disabled} />
                <div style={{ minHeight: "20px" }}>&nbsp;</div>
                <Box display="flex" style={{ overflow: 'hidden' }}>
                    <Route path="/" exact>
                        <Home
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/site">
                        <Site
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}

                        />
                    </Route>
                    <Route path="/site-update/:programId/:orgUnitId">
                        <UpdateSite
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/site-update/:id" >
                        <UpdateSite
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/site-view/:id/:view" >
                        <UpdateSite
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/site-edit/:id/:edit" >
                        <UpdateSite
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/site-history" >
                        <HistorySite
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/history/:id" exact>
                        <Historico
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            tabOrder={tabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                    <Route path="/new-event" >
                        <NewEvent
                            messageBox={popSnackbar}
                            setTabOrder={setTabOrder}
                            setdisabled={setdisabled}
                        />
                    </Route>
                </Box>
            </Router>
        </>
    );
}
