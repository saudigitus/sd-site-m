import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import TableHead from '@material-ui/core/TableHead';
import Export from './Export.component';
import { NavLink } from "react-router-dom";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import UpdateIcon from '@material-ui/icons/Update';
import SettingsIcon from '@material-ui/icons/Settings';
import HistoryIcon from '@material-ui/icons/Update';
import { useConfig } from '@dhis2/app-runtime'
import NewIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';


// import LinearProgress from '@material-ui/core/LinearProgress';

import {
    Box, Paper, Container, ListItemText,
    Fab, Tooltip, LinearProgress, Menu,
    TableSortLabel, Typography, Checkbox, MenuItem,
    FormControlLabel, ListItem, List, Popover, Button, Divider,
    ListItemIcon, Grid, TextField, InputAdornment
} from "@material-ui/core";

const useStyles1 = makeStyles((theme) => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
}));



const useStyles2 = makeStyles({
    table: {
        minWidth: 500,
    },
});
let a = 0
let total = 0
let size = 0
let page = 0
let dados = []
let q = []
let dadosGeral = []
let userDetails = []
export default function Table1(props) {
    const classes = useStyles2();
    // const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [anchorEl, setAnchorEl] = useState(null);
    const { baseUrl, apiVersion } = useConfig()
    const [query, setquery] = useState("")
    // useEffect(() => {
    //     props.data
    //     dados = []
    // }, [])
    // console.log(props?.data)

    size = props.size
    total = props.totalRow
    page = props.pages - 1
    dados = props?.data
    //  console.log(page , size)



    const emptyRows = size - Math.min(size, props.data.length - page * size);

    const handleChangePage = (e, page) => {
        props.changePage(page + 1)
        // console.log(page)
    }

    const handleChangeRowsPerPage = (event) => {
        // console.log(parseInt(event.target.value, 10))
        props.changeRowsPerPage(parseInt(event.target.value, 10));
        // setPage(0);
    };

    function search(rows) {
        // console.log(rows)
        return rows.filter(
            (row) =>
                row.provincia.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
                row.distrito.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
                row.orgUnitName.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
                row.lastUpdated.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
                row.code.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
                row.storedBy.toLowerCase().indexOf(query.toLowerCase()) > -1
        )
    }

    return (
        <div>

            <Grid item xs={12} sm container style={{ position: 'relative' }}>
                <Grid item xs container direction="column" spacing={2}>
                    <Grid item xs>
                        <Typography style={{ margin: 10 }} variant="h5">{props.title}</Typography>
                    </Grid>

                </Grid>
                <Grid item >

                    <Grid item xs={12} sm container style={{ position: 'relative' }}>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <TextField style={{ width: 230, marginRight: 20, marginBottom: 20, marginTop: 25 }} placeholder={"Pesquisar"} onChange={(e) => setquery(e.target.value)} margin="normal" InputProps={{
                                    startAdornment: <InputAdornment position="start"><SearchIcon fontSize={'small'} /></InputAdornment>,

                                }} />
                            </Grid>

                        </Grid>
                        <Grid item >

                            {props.isLoaded ? null :

                                <Export id={props.id} headers={props.headers} endDate={props.endDate} startDate={props.startDate} status={props.status} data={props.data} />
                            }
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>

            < TableContainer component={Paper} >
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <TableRow>
                            {props.columns.map((column) => (
                                <TableCell
                                    key={column.field}
                                // align={column.field}
                                // style={{ minWidth: column.minWidth }}
                                >
                                    {column.title}
                                </TableCell>
                            ))}
                            <TableCell
                                align={'right'}
                            >
                                <SettingsIcon fontSize="medium" />
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(search(props?.data)).slice(page * size, page * size + size).map((row) => (
                            <TableRow
                                hover
                                key={row.event}
                                tabIndex={-1}
                                role="checkbox"
                                style={{ cursor: 'pointer' }}
                            >
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                    {row.provincia}
                                </TableCell>
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                    {row.distrito}
                                </TableCell>
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                    {row.orgUnitName}
                                </TableCell>
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                    {row.code}
                                </TableCell>
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }} >
                                    {row.lastUpdated}
                                </TableCell>
                                {(props.status == 0) ?
                                    <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                        {row.attributeCategoryOptions}
                                    </TableCell>
                                    :
                                    null
                                }
                                <TableCell component={NavLink} to={`/site-view/${row.event}/true`} style={{ textDecoration: "none" }}>
                                    {(row.storedBy === "anipassa" || row.storedBy === "amuchanga" || row.storedBy === "zsaugene" || row.storedBy === "fcumaio") ? "Imported" : row.storedBy}
                                </TableCell>

                                <TableCell align="right">
                                    <IconButton aria-controls={`simple-menu-${row.event}`}
                                        aria-haspopup="true"
                                        onClick={e => { userDetails = row; setAnchorEl(e.currentTarget) }}
                                        size="small"><MoreVertIcon /></IconButton>
                                    <Menu
                                        elevation={0}
                                        id={`simple-menu-${row.event}`}
                                        anchorEl={anchorEl}
                                        keepMounted
                                        PaperProps={{
                                            style: {
                                                border: '1px solid #d3d4d5'
                                            },
                                        }}
                                        anchorOrigin={{
                                            horizontal: 'right',
                                        }}
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={Boolean(anchorEl)}
                                        onClose={() => setAnchorEl(null)}>
                                        <MenuItem component={NavLink} to={`/site-view/${userDetails['event']}/true`}>
                                            <ListItemIcon>
                                                <VisibilityIcon fontSize="small" />
                                            </ListItemIcon>
                                            <ListItemText primary="Visualizar" />
                                        </MenuItem>
                                        <MenuItem component={NavLink} to={`/site-update/${userDetails['event']}`}>
                                            <ListItemIcon>
                                                <NewIcon fontSize="small" />
                                            </ListItemIcon>
                                            <ListItemText primary="Adicionar novo assessment" />
                                        </MenuItem>
                                        <MenuItem component={NavLink} to={`/site-edit/${userDetails['event']}/true`}>
                                            <ListItemIcon>
                                                <HistoryIcon fontSize="small" />
                                            </ListItemIcon>
                                            <ListItemText primary="Actualizar o assessment" />
                                        </MenuItem>
                                        {/* {(props.status == 2) ?
                                            null
                                            :
                                            <MenuItem component={NavLink} to={`/history/${userDetails['orgUnit']}`}>
                                                <ListItemIcon>
                                                    <HistoryIcon fontSize="small" />
                                                </ListItemIcon>
                                                <ListItemText primary="Historico" />
                                            </MenuItem>
                                        } */}

                                    </Menu>
                                </TableCell>
                            </TableRow>
                        ))}

                        {/* {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )} */}
                    </TableBody>
                    <TableFooter>
                        {(props.status == 2) ?
                            null
                            :
                            <TablePagination
                                rowsPerPageOptions={[10, 25, { label: 'TODOS', value: search(props?.data) ? search(props?.data).length : total }]}
                                count={search(props?.data) ? search(props?.data).length : total}
                                rowsPerPage={size}
                                page={page}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                            />
                        }

                    </TableFooter>
                </Table>
            </TableContainer >
        </div >
    );
}