import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import SpeedDial from '@material-ui/lab/SpeedDial';
import Edit from '@material-ui/icons/Edit';
import { useConfig } from '@dhis2/app-runtime'
import { useHistory } from "react-router-dom";



const useStyles = makeStyles((theme) => ({
    exampleWrapper: {
        position: 'relative',
        marginTop: theme.spacing(3),
        height: 80,
    },
    speedDial: {
        position: 'absolute',
        '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
            top: theme.spacing(2),
            left: theme.spacing(2),
        },
    },
}));


export default function FloatButton(props) {
    const history =useHistory()
    const { baseUrl, apiVersion } = useConfig()
    const classes = useStyles();
    return (
        
            <div className={classes.exampleWrapper}>
                <SpeedDial
                    ariaLabel="Editar Descrição"
                    className={classes.speedDial}
                    icon={<Edit/>}
                    onClick={()=> window.open(baseUrl+"/dhis-web-maintenance/#/edit/indicatorSection/indicator/"+props.id, "_blank")}
                >
                
                </SpeedDial>
            </div>
      
    );
}