import 'date-fns';
import React, { useState } from 'react'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import { format, formatDistance, formatRelative, subDays, subYears } from 'date-fns'
import DateFnsUtils from '@date-io/date-fns';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider
} from '@material-ui/pickers';

const ListaUserOrgUnits = {
    results: {
        resource: 'me.json',
        params: {
            fields: ['organisationUnits[id,name,code,level,created]'],
            withinUserSearchHierarchy: true,
            paging: false,
        },
    },
}

const ListaPrograms = {
    results: {
        resource: 'programs.json',
        params: {
            paging: false,
        },
    },
}

const ListaOrgUnitsNivelAbaixo = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: ({ level }) => ({
            // level: level,
            fields: "children[name~rename(displayName),level,id]",
            paging: false,
        })
    }
}


const ListaOrgUnits = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: {
            // includeChildren: true,
            fields: "children[name~rename(displayName),level,id]",
            paging: false,
        },
    },
}
let oUabaixo = []
let MaisoUabaixo = []
export default function HeaderAction(props) {
    const { loading, data } = useDataQuery(ListaUserOrgUnits)
    // let programs = useDataQuery(ListaPrograms)
    const [loaded, setloaded] = useState(true)
    const [state, setstate] = useState(false)
    const [orgUnits, setorgUnits] = useState([])
    const [indicatorGroups, setIndicatorGroups] = React.useState('');
    const [indicatorTypes, setIndicatorTypes] = React.useState('');
    const [program, setprogram] = useState('')
    const [endDate, setendDate] = useState(new Date())
    const [startDate, setstartDate] = useState(format(subYears(new Date(), 1), 'yyyy-MM-dd'))
    const engine = useDataEngine()

    if (!loading) {
        let orgunits = engine.query(ListaOrgUnitsNivelAbaixo, { variables: { id: data?.results.organisationUnits[0].id, level: data?.results.organisationUnits[0].level } })

        Promise.all([orgunits]).then(res => {
            oUabaixo = res
            oUabaixo[0]?.results.children.splice(0, 0, { id: data?.results.organisationUnits[0].id, displayName: data?.results.organisationUnits[0].name })

        })

        let abc = engine.query(ListaOrgUnits, { variables: { id: data?.results.organisationUnits[0].id } })
        Promise.all([abc]).then(res => {
            res[0].results.children.splice(0, 0, { id: data?.results.organisationUnits[0].id, displayName: "TODOS" })
            MaisoUabaixo = res[0].results.children
            setloaded(false)
        })
    }

    const handleChangeGroups = (event) => {
        setIndicatorGroups(event.target.value);
        setloaded(true)
        setstate(true)
        let abc = engine.query(ListaOrgUnits, { variables: { id: event.target.value } })
        Promise.all([abc]).then(res => {
            res[0].results.children.splice(0, 0, { id: event.target.value, displayName: "TODOS" })
            setorgUnits(res[0].results.children)
            setloaded(false)
        })
    };
    //  console.log(programs)
    const handleChangeType = (event) => {
        setIndicatorTypes(event.target.value);
    };

    const handleChangeProgram = (event) => {
        setprogram(event.target.value);
    };
    if (loaded) {
        return (
            <CenteredContent>
                <CircularLoader />
            </CenteredContent>
        )
    }
    const handleDateChange = (date) => {
        setstartDate(date);
    };
    const handleEndDateChange = (date) => {
        setendDate(date);
    };

    return (
        <Grid container direction="column" >
            <Box display="flex">


                <TextField
                    id="standard-select-currency"
                    select
                    label="Unidade Organizacional"
                    style={{ marginLeft: 50, width: 250, fontSize: 15, fontFamily: 'roboto', marginTop: 15 }}
                    value={indicatorGroups === "" ? oUabaixo[0]?.results.children[0].id : indicatorGroups}
                    onChange={handleChangeGroups}
                // helperText="Porfavor selecione a Organização"
                >
                    {oUabaixo && (oUabaixo[0]?.results.children)?.map((post) => (
                        <MenuItem key={post.id} value={post.id}>
                            {post.displayName}
                        </MenuItem>
                    ))}
                </TextField>


                <TextField
                    id="standard-select-currency"
                    select
                    label="Nível abaixo"
                    // disabled={state}
                    style={{ marginLeft: 50, width: 250, fontSize: 15, fontFamily: 'roboto', marginTop: 15 }}
                    value={indicatorTypes === "" ? state ? orgUnits[0]?.id : MaisoUabaixo[0]?.id : indicatorTypes} 
                    onChange={handleChangeType}
                // helperText="Porfavor selecione a Organização"
                >
                    {(state ? orgUnits : MaisoUabaixo).map((post) => (
                        <MenuItem key={post.id} value={post.id}>
                            {post.displayName}
                        </MenuItem>
                    ))}
                </TextField>


                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        disableToolbar
                        style={{ marginLeft: 50, width: 250, fontSize: 15, fontFamily: 'roboto' }}
                        variant="inline"
                        format="yyyy-MM-dd"
                        margin="normal"
                        id="date-picker-inline"
                        label={"Periodo Inicial"}
                        value={startDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                    <KeyboardDatePicker
                        disableToolbar
                        style={{ marginLeft: 50, width: 250, fontSize: 15, fontFamily: 'roboto' }}
                        variant="inline"
                        format="yyyy-MM-dd"
                        margin="normal"
                        id="date-picker-inline1"
                        label={"Periodo Final"}
                        value={endDate}
                        onChange={handleEndDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                </MuiPickersUtilsProvider>
                <Box flexGrow={1}>&nbsp;</Box>
                <Box style={{ padding: '10px' }}>
                    <Button style={{ top: 14, marginRight: 50 }} variant="contained" typeof={"submit"} onClick={() => props.greetHandler(indicatorTypes === "" ? state ? orgUnits[0]?.id : MaisoUabaixo[0]?.id : indicatorTypes, startDate, endDate, data?.results.organisationUnits[0].id)} color="primary">
                        Exibir Sites
                         </Button>
                </Box>

            </Box>
        </Grid>
    )
}
