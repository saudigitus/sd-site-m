import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { Tabs, AppBar, Tab, Divider } from '@material-ui/core';


function TabSection(props) {
    const [tabOrder, setTabOrder] = useState(props.tabOrder);
    return (
        <AppBar style={{ color: '#000', position: 'relative', backgroundColor: '#ddd', width: '100%' }}>
            <Tabs
                indicatorColor="primary"
                // textColor="primary"
                value={props.tabOrder === 0 ? tabOrder : props.tabOrder}
                onChange={(event, value) => { setTabOrder(value) }}>
                <Tab component={Link} disabled={props.disabled} to="/" label={<span style={{ fontSize: '14px', color: 'rgba(0,0,0,0.6)' }}>TODOS</span>} />
                <Tab component={Link} disabled={props.disabled} to="/site" label={<span style={{ fontSize: '14px', color: 'rgba(0,0,0,0.6)' }}>LISTA DE SITES</span>} />
                <Tab component={Link} disabled={props.disabled} to="/site-history" label={<span style={{ fontSize: '14px', color: 'rgba(0,0,0,0.6)' }}>Histórico de Actualização</span>} />
                {/* <Tab component={Link} to="/update-site" label={<span style={{ fontSize: '14px', color: 'rgba(0,0,0,0.6)' }}>ACTUALIZAR SITES</span>} /> */}
            </Tabs>
        </AppBar>
    );
}

export default TabSection;