import React, { forwardRef, useState, useEffect } from 'react'
import Excel from 'exceljs'
import { saveAs } from 'file-saver'
import { useDataQuery, useDataEngine } from '@dhis2/app-runtime'
import { CenteredContent, CircularLoader } from '@dhis2/ui'
import { format, formatDistance, formatRelative, subDays, subYears } from 'date-fns'
import GetAppIcon from '@material-ui/icons/GetApp';
import {
    Box, Paper, Container, ListItemText,
    Fab, Tooltip, LinearProgress, Menu,
    TableSortLabel, Typography, Checkbox, MenuItem,
    FormControlLabel, ListItem, List, Popover, Button, Divider,
    ListItemIcon, Grid, TextField
} from "@material-ui/core";

const ListaTodosEvents = {
    results: {
        resource: "events.json",
        params: ({ id, endDate, startDate }) => ({
            program: "xW2D1dak6g6",
            orgUnit: [id],
            ouMode: "DESCENDANTS",
            order: "eventDate%3Adesc",
            totalPages: true,
            fields: 'event,orgUnit,lastUpdated,orgUnitName,dueDate,dataValues[dataElement,value]',
            endDate: endDate,
            startDate: startDate,
            paging: false
        })
    }
}

const OrgUnitsDeltalhe = {
    results: {
        resource: "organisationUnits",
        id: ({ id }) => id,
        params: {
            fields: 'code,ancestors[id,name]',
            paging: false,
        },
    },
}

const fieldsMapping = [
    {
        mainDateElement: "KLWeSxlzFfk", masterDataElement: "pqkZ5eRXQeU", value: "1"
    },
    {
        mainDateElement: "Ys5yIsJUJD4", masterDataElement: "pqkZ5eRXQeU", value: "0"
    },
    {
        mainDateElement: "vDkQtqhzYbT", masterDataElement: "uM6X63gpZf9", value: "0"
    },
    {
        mainDateElement: "AbHxI6ztCbw", masterDataElement: "QBCGkrVdrCN", value: "0"
    },
    {
        mainDateElement: "HU05vCWp17q", masterDataElement: "EbpxmdSa192", value: "0"
    },
    {
        mainDateElement: "eOm4bNyTAm3", masterDataElement: "HcsdMnMtOur", value: "0"
    },
    {
        mainDateElement: "TPAj0hNZZ2g", masterDataElement: "VPlQWsj7ZFq", value: "1"
    },
    {
        mainDateElement: "kC3JUSy1tzN", masterDataElement: "D9Ay6QmQVqM", value: "1"
    },
    {
        mainDateElement: "sO4F0BizoYz", masterDataElement: "M5FFNTKXSnm", value: "1"
    },
    {
        mainDateElement: "eOm4bNyTAm3", masterDataElement: "DYl0bZseIS3", value: "0"
    },

]


let a = 0
let total = 0
let size = 0
let page = 0
let dados = []
let q = []
let dadosGeral = []
let userDetails = []

export default function GenExcel(props) {
    const engine = useDataEngine()
    const [isLoaded, setIsLoaded] = useState(false);


    const checkId = (elem, dataValues) => {
        for (let fieldMapping of fieldsMapping) {
            if (elem == fieldMapping.mainDateElement) {
                for (let sectionDataElement of dataValues) {
                    if (fieldMapping.masterDataElement == sectionDataElement.dataElement) {
                        if (fieldMapping.value != sectionDataElement.value) {
                            return true
                        }
                    }
                }
            }
        }

        return false
    }

    useEffect(() => {
        dadosGeral = []
    }, [])

    const listaEvents = useDataQuery(ListaTodosEvents, {
        lazy: true,
        onComplete: function name(params) {
            if (params.results.events.length == 0) {
                setIsLoaded(false)
            }
            dados = []
            a = 0

            params.results.events.forEach((element) => {
                let abc = engine.query(OrgUnitsDeltalhe, { variables: { id: element.orgUnit } })
                Promise.all([abc]).then(resp => {
                    a = a + 1
                    for (let index = 0; index < element.dataValues.length; index++) {
                        if (element.dataValues[index].dataElement === "CF98KbVSMXO" || element.dataValues[index].dataElement === "eB1XOr5TYhs" || element.dataValues[index].dataElement === "Zm0GjduxXg3" || element.dataValues[index].dataElement === "xc8eGXgGB7p" || element.dataValues[index].dataElement === "OwfP3xXIQ4d" || element.dataValues[index].dataElement === "QenomDLaAvk" || element.dataValues[index].dataElement === "REufiDMg3jt" || element.dataValues[index].dataElement === "LVZcZ0fwCQ6" || element.dataValues[index].dataElement === "wP7EyLYxTrx") {
                            q = ({ ...q, [element.dataValues[index].dataElement]: parseInt(element.dataValues[index].value) })
                        } else {
                            let showValue = checkId(element.dataValues[index].dataElement, element.dataValues)
                            if (element.dataValues[index].dataElement === "KLWeSxlzFfk" || element.dataValues[index].dataElement === "Ys5yIsJUJD4" || element.dataValues[index].dataElement === "vDkQtqhzYbT" || element.dataValues[index].dataElement === "AbHxI6ztCbw" || element.dataValues[index].dataElement === "HU05vCWp17q" || element.dataValues[index].dataElement === "eOm4bNyTAm3" || element.dataValues[index].dataElement === "TPAj0hNZZ2g" || element.dataValues[index].dataElement === "kC3JUSy1tzN" || element.dataValues[index].dataElement === "sO4F0BizoYz") {
                                q = ({ ...q, [element.dataValues[index].dataElement]: showValue ? null : `${format(new Date(element.dataValues[index].value), 'yyyy-MM-dd')}` })
                            } else {
                                q = ({ ...q, [element.dataValues[index].dataElement]: showValue ? null : [element.dataValues[index].value].toString() })
                            }

                        }

                    }
                    q = ({ ...q, "OrgUnit": element.orgUnit })
                    q = ({ ...q, "orgUnitName": element.orgUnitName })
                    q = ({ ...q, "dataActualizacao": `${format(new Date(element.lastUpdated), 'yyyy-MM-dd')}` })
                    q = ({ ...q, "provincia": resp[0].results?.ancestors[1]?.name })
                    q = ({ ...q, "distrito": resp[0].results?.ancestors[2]?.name })
                    dadosGeral.push(q)

                    //Inicializar a variavel para a próxima interação
                    for (let index = 0; index < element.dataValues.length; index++) {
                        q = ({ ...q, [element.dataValues[index].dataElement]: "" })
                    }
                }).finally(() => {

                    if (a == params.results.events.length) {
                        setIsLoaded(false)
                        ExcelGenerator(dadosGeral)
                    }

                })
            })
        },
        onError: (res) => {
            console.log(res)
            setIsLoaded(false)
        }
    })


    const ExcelGenerator = async (rows) => {
        const workbook = new Excel.Workbook();
        const worksheet = workbook.addWorksheet("Sites");
        worksheet.columns = props.headers

        worksheet.getRow('1').font = {
            size: 10,
            bold: true,
        };
        rows.map((row) => {
            worksheet.addRow(row)
        })

        const buf = await workbook.xlsx.writeBuffer()
        saveAs(new Blob([buf]), 'ListaDeSites.xlsx')
        dadosGeral = []
    }


    const VerifyExcel = () => {
        setIsLoaded(true)
        if (props.status == 0 || props.status == 2) {

            props.data.forEach((element) => {

                for (let index = 0; index < element.dataValues.length; index++) {
                    if (element.dataValues[index].dataElement === "CF98KbVSMXO" || element.dataValues[index].dataElement === "eB1XOr5TYhs" || element.dataValues[index].dataElement === "Zm0GjduxXg3" || element.dataValues[index].dataElement === "xc8eGXgGB7p" || element.dataValues[index].dataElement === "OwfP3xXIQ4d" || element.dataValues[index].dataElement === "QenomDLaAvk" || element.dataValues[index].dataElement === "REufiDMg3jt" || element.dataValues[index].dataElement === "LVZcZ0fwCQ6" || element.dataValues[index].dataElement === "wP7EyLYxTrx") {
                        q = ({ ...q, [element.dataValues[index].dataElement]: parseInt(element.dataValues[index].value) })
                    } else {
                        let showValue = checkId(element.dataValues[index].dataElement, element.dataValues)
                        if (element.dataValues[index].dataElement === "KLWeSxlzFfk" || element.dataValues[index].dataElement === "Ys5yIsJUJD4" || element.dataValues[index].dataElement === "vDkQtqhzYbT" || element.dataValues[index].dataElement === "AbHxI6ztCbw" || element.dataValues[index].dataElement === "HU05vCWp17q" || element.dataValues[index].dataElement === "eOm4bNyTAm3" || element.dataValues[index].dataElement === "TPAj0hNZZ2g" || element.dataValues[index].dataElement === "kC3JUSy1tzN" || element.dataValues[index].dataElement === "sO4F0BizoYz") {
                            q = ({ ...q, [element.dataValues[index].dataElement]: showValue ? null : `${format(new Date(element.dataValues[index].value), 'yyyy-MM-dd')}` })
                        } else {
                            q = ({ ...q, [element.dataValues[index].dataElement]: showValue ? null : [element.dataValues[index].value].toString() })
                        }
                    }
                }
                q = ({ ...q, "orgUnitName": element.orgUnitName })
                q = ({ ...q, "OrgUnit": element.orgUnit })
                q = ({ ...q, "dataActualizacao": `${format(new Date(element.lastUpdated), 'yyyy-MM-dd')}` })
                q = ({ ...q, "provincia": element.provincia })
                q = ({ ...q, "distrito": element.distrito })
                dadosGeral.push(q)

                //Inicializar a variavel para a próxima interação
                for (let index = 0; index < element.dataValues.length; index++) {
                    q = ({ ...q, [element.dataValues[index].dataElement]: "" })
                }
            })

            setIsLoaded(false)
            ExcelGenerator(dadosGeral)
        } else {
            if (props.startDate == "") {
                setIsLoaded(true)
                listaEvents.refetch({ id: props.id, startDate: format(subYears(new Date(), 1), 'yyyy-MM-dd'), endDate: format(new Date(), 'yyyy-MM-dd') })
            } else {
                setIsLoaded(true)
                listaEvents.refetch({ id: props.id, startDate: format(new Date(props.startDate), 'yyyy-MM-dd'), endDate: format(new Date(props.endDate), 'yyyy-MM-dd') })
            }
        }

    }


    return (
        <div>
            {(isLoaded) ?
                <CenteredContent>
                    <CircularLoader />
                </CenteredContent> :
                <Button onClick={VerifyExcel} style={{ margin: 15 }}><GetAppIcon /></Button>
            }
        </div>
    )
}
