import React from 'react';
import { Link, NavLink } from "react-router-dom";
import { Card, CardActionArea, Typography, Divider } from '@material-ui/core';

const Cartao = (props) => {
    return (
        <Card elevation={2} style={{ width: 230, height: 196, borderRadius: 0 }}>
            <CardActionArea component={NavLink} to={props.url}>
                <Typography variant="button" display="block" style={{ marginTop: "17px", marginLeft: 8 }}>
                    {props.title}
                </Typography>
                <Divider />
                <Typography style={{ minHeight: 120, padding: '8px', fontSize:16 }} variant="body2"
                    color="textSecondary" component="p">
                    {props.description}
                </Typography>
            </CardActionArea>
        </Card>
    );
}

export default Cartao;