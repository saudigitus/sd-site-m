import React, { useState }  from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
// import FloatButton from './FloatButton'




export default function TableExpanded(props) {
    const [rows] = useState(props.array)
    return (
            <Container maxWidth="xl" maxHeight="xl" > 
               <h2>Nome Curto</h2>
               {rows.shortName}
               <h2>Numerador</h2>
               {rows.numerator}
               <h2>Denominador</h2>
               {rows.denominator}
               <h2>Descrição</h2>
               {rows.description}
                {/* <FloatButton id={rows.id}/> */}
            </Container>
    )
}
