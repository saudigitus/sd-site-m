const config = {
    type: 'app',
    name: 'ECHO Site Mapping',
    title: 'ECHO Site Mapping',
    description:'',

    entryPoints: {
        app: './src/App',
    },
}

module.exports = config
